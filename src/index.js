import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import customer from './ducks/Customer';
import cousines from './ducks/Cousine';
import stores from './ducks/Store';

const reducers = combineReducers({
  customer,
  cousines,
  stores,
});

const enchancers = composeWithDevTools(applyMiddleware(thunk));

const store = createStore(reducers, {}, enchancers);

const ComposedApp = () => (
  <Provider store={store} >
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(<ComposedApp />, document.getElementById('root'));
registerServiceWorker();
