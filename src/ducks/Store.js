import { apiFetch } from '../helpers/api';

const types = {
  setStores: 'STORES/SET',
  clearStores: 'STORES/CLEAR',
};

export const fetchStores = (cousineId) => (dispatch) => {
  return apiFetch(`/Cousine/${cousineId}/stores`)
  .then(data => dispatch(setStores(data)))
  .catch(err => console.log(err));
}

export const clearStores = () => (dispatch) => {
  dispatch({
    type: types.clearStores,
  });
}

export const setStores = (stores) => (dispatch) => {
  return dispatch({
    type: types.setStores,
    stores,
  });
}

export default (state = [], action) => {
  switch (action.type) {
    case types.setStores:
      return action.stores;

    case types.clearStores:
      return [];

    default:
      return state;
  }
}
