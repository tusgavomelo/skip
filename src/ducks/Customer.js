import { apiFetch } from '../helpers/api';
import { setToken, getToken, removeToken } from '../helpers/token';

const types = {
  setCustomer: 'CUSTOMER/SET',
};

export const signUpCustomer = (customer) => (dispatch) => {
  return apiFetch('/Customer', {
    method: 'POST',
    body: JSON.stringify(customer),
  })
  .then((response) => {
    if (response.error) {
      throw response.error;
    }
    return response;
  })
  .then((token) => setToken(token))
  .then(() => dispatch(setCustomer(customer)))
  .catch((err) => console.log('catch', err));
};

export const loginCustomer = (customer) => (dispatch) => {
  return apiFetch(`/Customer/auth?email=${customer.email}&password=${customer.password}`, {
    method: 'POST',
  })
  .then((response) => {
    if (response.error) {
      throw response.error;
    }
    return response;
  })
  .then((token) => setToken(token))
  .then(() => dispatch(setCustomer(customer)))
};

export const fetchAuthenticatedCustomer = () => (dispatch) => {
  dispatch(setCustomer({
    email: getToken() || false
  }));
};

export const logoutCustomer = () => (dispatch) => {
  removeToken();
  dispatch(setCustomer({}));
};

export const setCustomer = (customer) => (dispatch) => {
  return dispatch({
    type: types.setCustomer,
    customer: {
      authorized: !!customer.email
    },
  });
};

export default (state = {}, action) => {
  switch (action.type) {
    case types.setCustomer:
      return action.customer;

    default:
      return state;
  }
}
