import { apiFetch } from '../helpers/api';

const types = {
  setCousines: 'COUSINE/SET',
};

export const fetchCousines = () => (dispatch) => {
  return apiFetch('/Cousine')
  .then(data => dispatch(setCousines(data)))
  .catch(err => console.log(err));
}

export const setCousines = (cousines) => (dispatch) => {
  return dispatch({
    type: types.setCousines,
    cousines,
  });
}

export default (state = [], action) => {
  switch (action.type) {
    case types.setCousines:
      return action.cousines;

    default:
      return state;
  }
}
