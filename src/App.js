import React, { Component } from 'react';
import { Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Home, SignUp, LogIn, Store } from './pages';
import Header from './components/Header';
import { fetchAuthenticatedCustomer } from './ducks/Customer';
import './App.css';

class App extends Component {
  componentDidMount() {
    this.props.fetchAuthenticatedCustomer();
  }

  render() {
    return (
      <div>
        <Header />
        <main>
          <Route exact path="/signup" component={SignUp}/>
          <Route exact path="/login" component={LogIn}/>
          <Route exact path="/" component={Home}/>
          <Route path="/cousine/:cousineId" component={Home}/>
          <Route path="/store/:cousineId" component={Store}/>
        </main>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  fetchAuthenticatedCustomer: bindActionCreators(fetchAuthenticatedCustomer, dispatch),
})

export default withRouter(connect(null, mapDispatchToProps)(App));
