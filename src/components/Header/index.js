import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { logoutCustomer } from '../../ducks/Customer';
import AuthenticatedCustomer from './AuthenticatedCustomer';
import NotAuthenticatedCustomer from './NotAuthenticatedCustomer';
import './header.css';

class Header extends React.Component {
  render() {
    const { customer, logoutCustomer } = this.props;

    return (
      <header>
        <h2>Skip</h2>
        <nav>
          {customer.authorized &&
            <AuthenticatedCustomer logoutCustomer={logoutCustomer} />
          }
          {!customer.authorized &&
            <NotAuthenticatedCustomer />
          }
        </nav>
      </header>
    )
  }
};

const mapDispatchToProps = (dispatch) => ({
  logoutCustomer: bindActionCreators(logoutCustomer, dispatch),
});

const mapStateToProps = (state) => ({
  customer: state.customer,
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
