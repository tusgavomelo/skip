import React from 'react';
import { Link } from 'react-router-dom';
import Button from '../@ui/Button';

const AuthenticatedCustomer = ({ logoutCustomer }) => (
  <div>
    <span>Authorized User</span>
    <Link to={`/`}>Home</Link>
    <Button onClick={logoutCustomer} text="Logout" />
  </div>
);

export default AuthenticatedCustomer;
