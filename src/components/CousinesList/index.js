import React from 'react';
import { Link } from 'react-router-dom';
import './Cousines.css';

const CousineItem = ({ id, name, active }) => (
  <li className={active ? 'active' : ''}>
    <Link to={`/cousine/${id}`}>
      {name}
    </Link>
  </li>
);

class CousinesList extends React.Component {
  render() {
    const { cousines, currentCousine } = this.props;

    return (
      <ul className="cousines-list">
        {cousines.map(cousine =>
          <CousineItem
            key={cousine.id}
            id={cousine.id}
            active={cousine.id === parseInt(10, currentCousine)}
            name={cousine.name}
          />
        )}
      </ul>
    );
  }
}

export default CousinesList;
