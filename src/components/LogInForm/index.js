import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loginCustomer } from '../../ducks/Customer';

import InputText from '../@ui/InputText';
import Button from '../@ui/Button';

class LogInForm extends React.Component {
  state = {
    err: false
  };

  onChangeInput = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.props.loginCustomer(this.state)
    .then(() => {
      console.log('logged in')
    })
    .catch((err) => this.setState({
      err
    }))
  };

  render() {
    const { err } = this.state;

    return(
      <form onSubmit={this.onSubmit}>
        <InputText name="email" label="E-mail" placeholder="Your e-mail" onChange={this.onChangeInput} />
        <InputText password name="password" label="Password" placeholder="Your strong password" onChange={this.onChangeInput} />
        {err &&
          <div>
            {err}
          </div>
        }
        <Button type="submit" text="Sign in" />
      </form>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  loginCustomer: bindActionCreators(loginCustomer, dispatch),
});

export default connect(null, mapDispatchToProps)(LogInForm);
