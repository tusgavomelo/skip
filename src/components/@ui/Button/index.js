import React from 'react';
import './Button.css';

export default ({ type, text, onClick }) => (
  <button type={type || 'button'} onClick={onClick}>
    {text}
  </button>
);
