import React from 'react';
import './InputText.css';

export default ({ name, placeholder, label, password, onChange }) => (
  <div className="input-text">
    <label htmlFor={name}>{label}</label>
    <input
      type={password ? "password" : "text"}
      placeholder={placeholder}
      name={name}
      onChange={onChange}
    />
  </div>
);
