import React from 'react';
import { Link } from 'react-router-dom';
import './StoresList.css';

const StoreItem = ({ id, name }) => (
  <li>
    <Link to={`/store/${id}`}>
      {name}
    </Link>
  </li>
);

class StoresList extends React.Component {
  render() {
    const { stores } = this.props;

    return (
      <div className="stores-container">
        <h3>Restaurants</h3>

        <ul className="stores-list">
          {stores.map(store =>
            <StoreItem key={store.id} id={store.id} name={store.name} />
          )}
        </ul>
      </div>
    );
  }
}

export default StoresList;
