import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { signUpCustomer } from '../../ducks/Customer';

import InputText from '../@ui/InputText';
import Button from '../@ui/Button';

class SignUpForm extends React.Component {
  onChangeInput = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.props.signUpCustomer(this.state);
  };

  render() {
    return(
      <form onSubmit={this.onSubmit}>
        <InputText name="name" label="Name" placeholder="Your name" onChange={this.onChangeInput} />
        <InputText name="address" label="Address" placeholder="Your address" onChange={this.onChangeInput} />
        <InputText name="email" label="E-mail" placeholder="Your e-mail" onChange={this.onChangeInput} />
        <InputText password name="password" label="Password" placeholder="Your strong password" onChange={this.onChangeInput} />
        <Button type="submit" text="Sign in" />
      </form>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  signUpCustomer: bindActionCreators(signUpCustomer, dispatch),
});

export default connect(null, mapDispatchToProps)(SignUpForm);
