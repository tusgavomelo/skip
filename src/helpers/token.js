import Cookies from 'js-cookie';

export const getToken = () => (
  Cookies.get('skip_auth_token')
);

export const setToken = (token) => {
  Cookies.set('skip_auth_token', token)
  return true;
}

export const removeToken = () => (
  Cookies.remove('skip_auth_token')
);
