import { getToken } from './token';

export const apiFetch = (path, { method, body, customHeaders } = {}) => {
  const headers = new Headers({
    'authorization': getToken(),
    'accept': 'application/json',
    'Content-Type': 'application/json-patch+json',
    ...customHeaders,
  });

  return fetch(`http://api-vanhack-event-sp.azurewebsites.net/api/v1${path}`, {
    headers,
    method,
    body,
  })
  .then((data) => data.json())
  .catch((err) => console.log('catch', err));
}
