import React from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import SignUpForm from '../components/SignUpForm';

class SignUp extends React.Component {
  render() {
    const { authorized } = this.props;

    return(
      <div>
        {authorized &&
          <Redirect to='/' />
        }
        Sign up
        <SignUpForm />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  authorized: state.customer.authorized,
})

export default connect(mapStateToProps, null )(SignUp);
