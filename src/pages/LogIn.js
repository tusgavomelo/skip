import React from 'react';
import { Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import LogInForm from '../components/LogInForm';

class LogIn extends React.Component {
  render() {
    const { authorized } = this.props;
    console.log('render login', authorized);
    return(
      <div>
        {authorized &&
          <Redirect to='/' />
        }
        Sign in
        <LogInForm />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  authorized: state.customer.authorized,
})

export default withRouter(connect(mapStateToProps, null)(LogIn));
