import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchCousines } from '../ducks/Cousine';
import { fetchStores, clearStores } from '../ducks/Store';
import CousinesList from '../components/CousinesList';
import StoresList from '../components/StoresList';

class Home extends React.Component {
  componentDidMount() {
    this.props.fetchCousines();
  }

  componentWillReceiveProps(nextProps) {
    const { stores, match } = this.props;
    const { cousineId } = match.params;
    const nextCousineId = nextProps.match.params.cousineId;

    // I'm sorry for these if's, I'm in hurry
    if (cousineId) {
      if (cousineId !== nextCousineId || !stores.length) {
        this.props.fetchStores(nextCousineId);
      }
    } else {
      if (cousineId !== nextCousineId) {
        this.props.clearStores();
      }
    }
  }

  render() {
    const { cousines, stores, match } = this.props;
    const { cousineId } = match.params;

    return (
      <div>
        <CousinesList cousines={cousines} currentCousine={cousineId} />
        {!cousineId &&
          <h2>Please select a Cousine</h2>
        }
        <StoresList stores={stores} />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  fetchCousines: bindActionCreators(fetchCousines, dispatch),
  fetchStores: bindActionCreators(fetchStores, dispatch),
  clearStores: bindActionCreators(clearStores, dispatch),
});

const mapStateToProps = (state) => ({
  cousines: state.cousines,
  stores: state.stores,
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
