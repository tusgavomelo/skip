Hello!

Unfourtunelly I couldn't finish the challenge. But here are the features that you can do with this app:

- Sign up
- Log in
- Select any Cousine
- View all restaurants for that Cousine

I made it using create-react-app, basically together with redux (and some middlewares) and react-router.
The next steps would be create a page for the restaurants and a list of products.

I also didn't had enough time to write unit tests. I would do it with jest and enzyme because they are great!

Hope you enjoy it, the hackathon was awesome. Thank you all!

How to run it:
```yarn install```
```yarn start```

Gustavo de Castro Melo
gcastromelo94@gmail.com
